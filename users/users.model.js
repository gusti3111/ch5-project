
const db = require('../db/models/');
const md5 = require('md5')


class usersModel{
    // get all users data
    getAllUsers = async() =>{
        const users = await db.UserGame.findAll({
              })
        return users;
      }
    //   data is exist or not
    dataIsExist = async(data) =>{
        const exist = await  db.UserGame.findOne({
            where :{username:data.username}
            })
            console.log(exist)
         
        if (exist) {
            return true;
            
        } else {
            return false
            
        }

    }   

    // record Data
    pushData = async(data) =>{
      const pushUser= await db.UserGame.create({
    
                        username:data.username,
                        email: data.email,
                        // encode password with md5
                        password: md5(data.password),
                        
                      
                    });
        return pushUser;

    }

    //login verification 
    verifiyLogin = async(username,password) =>{
        const findData = await  db.UserGame.findOne({
            where :{username:username, password:md5(password)}
            })
            // console.log(user.username)
            //     return user.username === username 
            
            //         user.password === md5(password)
         
        return findData;

    }
    // delete user
    deleteUser = async(id) => {
        const rowsDeleted = await db.UserGame.destroy({
        where: {
            id : id
        }
        });
        console.log(rowsDeleted)
        return rowsDeleted;
    }


}
module.exports =  new usersModel();
