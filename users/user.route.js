const express =require('express')
const usersController = require('./users.controller')

const userRouter = express.Router()


userRouter.get('/',usersController.getAll)

// getbyid 
userRouter.get('/:id',usersController.getbyId)
userRouter.post('/signup', usersController.signup)


userRouter.post('/login',usersController.userslogin)
userRouter.delete('/:id',usersController.deleteRecord)



module.exports =  userRouter;