const usersModel = require('./users.model')


class usersController{
    getAll = async(req,res) =>{
        const getAll = await usersModel.getAllUsers()
        return res.json(getAll)

    }
    signup = async(req,res) =>{
        const data = req.body
        // data validation before post to users 
        if (data.username  == "" || data.username === undefined) {
           return  res.status(404).json({ message: "username not found"})
            
        }if (data.email === '' || data.email === undefined) {
            
           return  res.status(404).json({ message: "email not found"})
    
        }if (data.password === "" || data.password === undefined){
           return res.status(404).json({ message: "password not found"})
            
          
        }
        const IsExist = await usersModel.dataIsExist[data]
        console.log(IsExist)
    
        if (IsExist) {
          
           return res.json({message: "Username already exists"})
            
        }
            usersModel.pushData(data);
            // status success code and response while post to users success
           return res.status(200).json({ message: "Success to insert new users" });  
       
   

        
    
    
      
        


  
    }
    getbyId = () =>{
        
    }
    userslogin = async(req,res) =>{
        const {username,password} = req.body;
            // const sortedValue = users.find((user) =>{
            //  return user.username === username &&
            //         user.password === md5(password)
            // })
        const findData =  await usersModel.verifiyLogin(username,password)
            // if(findData === undefined){
            //     res.statusCode = 404;
            //     return res.json({message: "Credential not found"})
            // }else{
            //     res.statusCode = 200;
            //     return res.json(findData)

        // }
        if (findData) {
           return res.status(200).json(findData)
            
        } else {
            return res.status(404).json({message:"Credential not found"})
            
        }
    
    

    }

    deleteRecord = (req, res) => {
        
        try {
            const id = req.body.id
            
            const rowsDeleted = usersModel.deleteUser(id);
            console.log(rowsDeleted)
         
            if (rowsDeleted === 0) {
            return res.status(404).json({ message: 'Record not found' });
            }else{
    
                return res.status(200).json({ message: 'Record deleted' });
            }
        } catch (error) {
            console.error('Error deleting record:', error);
            res.status(500).json({ message: 'Internal server error' });
        }
}
   
}

module.exports = new usersController();
