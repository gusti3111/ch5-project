'use strict';

const { DataTypes } = require('sequelize');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    // kalo id kan auto increment sedangkan kalo user_id ini auto increment juga atau kalo kita post ke database harus di tulis juga user_idnya
    await queryInterface.addColumn('user_biodata','user_id',{
      type:DataTypes.INTEGER
    })
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn('user_biodata','user_id')
  }
};
