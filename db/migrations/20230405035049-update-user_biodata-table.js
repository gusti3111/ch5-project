'use strict';

const { DataTypes } = require('sequelize');
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
  
    await queryInterface.addColumn('user_biodata','email',{
      type: DataTypes.STRING

    })
    await queryInterface.addColumn('user_biodata','fullname',{
      type:DataTypes.STRING
    })
    await queryInterface.addColumn('user_biodata','password',{
      type:DataTypes.STRING
    })
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
