const Pool = require('pg').Pool;

const pool = new Pool({
    user: 'postgres',
    host: '127.0.0.1',
    database: 'products',
    password: '12345',
    port: 5438, // default PostgreSQL port
  });

  module.exports= pool;