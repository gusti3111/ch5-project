
const prodModel = require('./product.model')
const pool = require('../db')

class prodControllers{
    getAllprod = (req,res) =>{
        
        pool.query(prodModel.getAllProducts, (err, results) => {
            if (err) throw err;
            res.json(results.rows)
        });
    }
    getprodbyId = (req,res) =>{
        const id = parseInt(req.params.id);

       const getid =  pool.query(prodModel.getProductById,[id],(err,results)=>{
                        if (err){
                            console.log(err)
                        }else{
                
                            res.status(200).json(results.rows)
                        }
                    })
        return getid
    }


    addproduct = (req,res) =>{
        const{name,price,stock} = req.body;

        const addprod = pool.query(prodModel.addProduct,[name,price,stock],(err,results) =>{
            if (err){
                throw err
            }else{
                res.status(201).json({message:"success Added product to database products"})
            }
        })
        const checkname = pool.query(prodModel.checkName ,[name],(err,results) =>{
                if(results.rows.length){
        
                    res.status(404).json({message:"Name already exists"})
                    return checkname
                }else{
                    return addprod
                }

        })

    }

    deleteProduct  = (req,res) =>{
        const id = parseInt(req.params.id)
    

        const deleteproduct = pool.query(prodModel.deleteProduct,[id],(err,results) =>{
            if (err){
                throw err
            }else{
        
                res.status(200).json({message:"Product deleted"})

            }
          
        })
        return deleteproduct
    }
    updateproduct = (req,res) =>{
        const id = parseInt(req.params.id)
        const{name,price,stock} = req.body;
        const updateproduct  = pool.query(prodModel.updateProduct,[name,price,stock,id],(err,results) =>{
            if (err){
                throw err
            } else{
                res.status(200).json({message:"Product Update successfully"})
            }
        })
        const checkdata =  pool.query(prodModel.getProductById,[id],(err,results)=>{
      
            if (!results.rows.length){
                res.status(404).json({message:"data not found"})
        
            }else{
                return updateproduct

            }
        })
    }


    
}

module.exports = new prodControllers();