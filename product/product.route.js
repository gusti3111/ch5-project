const express = require('express')
const prodControllers = require('./product.controller')
const prodRouter = express.Router()

// getall product 
prodRouter.get('/',prodControllers.getAllprod)
// getprodbyID
prodRouter.get('/:id',prodControllers.getprodbyId)
// add new product
prodRouter.post("/addproduct",prodControllers.addproduct)
// Update product
prodRouter.put("/:id",prodControllers.updateproduct)
// deleteproduct
prodRouter.delete("/id",prodControllers.deleteProduct)
module.exports = prodRouter;
